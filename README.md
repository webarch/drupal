Ansible role to install [Drupal](https://www.drupal.org/) sites using [Composer](https://getcomposer.org/) and [Drupal Console](https://drupalconsole.com/) using [drupal/recommended-project](https://www.drupal.org/docs/develop/using-composer/using-composer-to-install-drupal-and-manage-dependencies#s-using-drupalrecommended-project).

This role has been designed to be use via the [users role](https://git.coop/webarch/users), however if all the required variables are defined that it should work without it, however this hasn't yet been documented ot tested.

This role requires the MariaDB / MySQL database to already have been created, the [MariaDB role](https://git.coop/webarch/mariadb) can be used for this. In addition [Composer](https://git.coop/webarch/composer) and [Drupal console](https://git.coop/webarch/drupal-console) and required. 
